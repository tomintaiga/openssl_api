#include "tools.h"
#include <openssl/err.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <fcntl.h>

namespace Taigasystem {

void InitOpenSSL()
{
    OpenSSL_add_all_algorithms();
    ERR_load_BIO_strings();
    ERR_load_crypto_strings();
    SSL_load_error_strings();
    SSL_library_init();

    std::cout << "Библиотека OpenSSL инициализирована\n";
}

void ClearOpenSSL()
{
    EVP_cleanup();
    CRYPTO_cleanup_all_ex_data();
    ERR_remove_thread_state(nullptr);
    ERR_free_strings();

    std::cout << "Библиотека OpenSSL очищена\n";
}

SSL_CTX_PTR CreateServerContext()
{
    return CreateContext(SSLv23_server_method());
}

SSL_CTX_PTR CreateClientContext()
{
    return CreateContext(SSLv23_client_method());
}

SSL_CTX_PTR CreateContext(const SSL_METHOD *method)
{
    if (method == nullptr)
    {
        std::cerr << "Не задан контекст\n";
        return nullptr;
    }

    SSL_CTX *ctx = nullptr;
    ctx = SSL_CTX_new(method);
    if (ctx == nullptr)
    {
        std::cerr << "Не могу создать SSL контекст:"
                  << ERR_reason_error_string(ERR_get_error()) << std::endl;
        return nullptr;
    }

    std::cout << "SSL контекст создан\n";

    std::shared_ptr<SSL_CTX> context(ctx, [](SSL_CTX *cur) {
        if (cur != nullptr)
        {
            SSL_CTX_free(cur);
            std::cout << "SSL контекст удален\n";
        }
    });
    return context;
}

SSL_PTR GetSSL(SSL_CTX_PTR ctx)
{
    if (ctx == nullptr)
    {
        std::cerr << "Не задан контекст для получения SSL\n";
        return nullptr;
    }

    SSL *_ssl = nullptr;
    _ssl = SSL_new(ctx.get());
    if (_ssl == nullptr)
    {
        std::cerr << "Не могу создать SSL из контекста:"
                  << ERR_reason_error_string(ERR_get_error()) << std::endl;
        return nullptr;
    }

    SSL_PTR ssl(_ssl, [](SSL *cur) {
        if (cur != nullptr)
        {
            SSL_free(cur);
            std::cout << "SSL структура удалена\n";
        }
    });

    return ssl;
}

SSL_PKEY_PTR GenerateKey(int size)
{
    //Создаем структуру для хранения ключей
    EVP_PKEY *_key = nullptr;
    _key = EVP_PKEY_new();
    if (_key == nullptr)
    {
        std::cout << "Ошибка: " << ERR_error_string(ERR_get_error(), nullptr) << std::endl;
        return nullptr;
    }

    std::cout << "Структура хранения ключей создана " << std::endl;

    std::shared_ptr<EVP_PKEY> key(_key,
                                  [](EVP_PKEY *cur) {
                                      if (cur != nullptr)
                                      {
                                          EVP_PKEY_free(cur);
                                          std::cout << "Структура для хранения ключей удалена\n";
                                      }
                                  });

    //Создаем структуру для больших чисел
    BIGNUM *_big = nullptr;
    _big = BN_new();
    if (_big == nullptr)
    {
        std::cout << "Ошибка: " << ERR_error_string(ERR_get_error(), nullptr) << std::endl;
        return nullptr;
    }

    std::cout << "Создана структура BIGNUM" << std::endl;
    std::shared_ptr<BIGNUM> big(_big, BN_free);

    //Настраиваем рахмер структуры
    if (BN_set_word(big.get(), RSA_F4) != 1)
    {
        std::cout << "Ошибка: " << ERR_error_string(ERR_get_error(), nullptr) << std::endl;
        return nullptr;
    }

    std::cout << "Изменен размер структуры BIGNUM" << std::endl;

    //Создаем структуру для ключей
    RSA *rsa = nullptr;
    rsa = RSA_new();
    if (rsa == nullptr)
    {
        std::cout << "Ошибка: " << ERR_error_string(ERR_get_error(), nullptr) << std::endl;
        return nullptr;
    }

    std::cout << "Создана RSA структура" << std::endl;

    //Генерация ключей
    if (RSA_generate_key_ex(rsa, size, big.get(), nullptr) != 1)
    {
        std::cout << "Ошибка: " << ERR_error_string(ERR_get_error(), nullptr) << std::endl;
        RSA_free(rsa);
        return nullptr;
    }

    std::cout << "Ключи сгенерированы" << std::endl;

    //Убираем ключи в структуру EVP
    if (EVP_PKEY_assign_RSA(key.get(), rsa) != 1)
    {
        std::cout << "Ошибка: " << ERR_error_string(ERR_get_error(), nullptr) << std::endl;
        RSA_free(rsa);
        return nullptr;
    }

    std::cout << "Ключи убраны в EVP" << std::endl;
    return key;
}

bool SaveKey(SSL_PKEY_PTR key, const std::string &fileName, const std::string &passwd)
{
    FILE *f = fopen(fileName.c_str(), "wb");
    if (f == nullptr)
    {
        std::cerr << "Не могу открыть файл: " << strerror(errno) << std::endl;
        return false;
    }

    if (passwd.empty())
    {
        if (PEM_write_PrivateKey(f, key.get(), nullptr, nullptr, 0, nullptr, nullptr) != 1)
        {
            fclose(f);
            std::cerr << "Ошибка: " << ERR_error_string(ERR_get_error(), nullptr) << std::endl;
            return false;
        }
    }
    else
    {
        if (PEM_write_PrivateKey(f,
                                 key.get(),
                                 EVP_aes_256_cbc(),
                                 (unsigned char *)passwd.c_str(),
                                 (int)passwd.length(),
                                 nullptr,
                                 nullptr) != 1)
        {
            fclose(f);
            std::cerr << "Ошибка: " << ERR_error_string(ERR_get_error(), nullptr) << std::endl;
            return false;
        }
    }

    fclose(f);

    std::cout << "Ключ успешно записан в " << fileName << std::endl;
    return true;
}

SSL_PKEY_PTR LoadKey(const std::string &fileName, const std::string &passwd)
{
    FILE *f = fopen(fileName.c_str(), "rb");
    if (f == nullptr)
    {
        std::cerr << "Не могу открыть файл: " << strerror(errno) << std::endl;
        return nullptr;
    }

    EVP_PKEY *key = nullptr;

    if (passwd.empty())
    {
        key = PEM_read_PrivateKey(f, nullptr, nullptr, nullptr);
        if (key == nullptr)
        {
            fclose(f);
            std::cerr << "Ошибка: " << ERR_error_string(ERR_get_error(), nullptr) << std::endl;
            return nullptr;
        }
    }
    else
    {
        auto *pass = new char[passwd.length()];
        memset(pass, 0, passwd.length());
        memcpy(pass, passwd.c_str(), passwd.length());

        key = PEM_read_PrivateKey(f, nullptr, nullptr, pass);
        if (key == nullptr)
        {
            fclose(f);
            delete[] pass;
            std::cerr << "Ошибка: " << ERR_error_string(ERR_get_error(), nullptr) << std::endl;
            return nullptr;
        }

        delete[] pass;
    }

    std::cout << "Ключ успешно загружен из " << fileName << std::endl;

    SSL_PKEY_PTR res(key, [](EVP_PKEY *cur) {
        if (cur != nullptr)
        {
            EVP_PKEY_free(cur);
            std::cout << "Структура для хранения ключей удалена\n";
        }
    });
    return res;
}

X509_REQ_PTR GenerateCSR(SSL_PKEY_PTR key, const SubjectMap &subj)
{
    if (key == nullptr)
    {
        std::cerr << "Не задан ключ для подписи запроса\n";
        return nullptr;
    }

    if (subj.empty())
    {
        std::cerr << "Не заданы данные запроса сертификата\n";
        return nullptr;
    }

    // Создаем структуру запроса
    X509_REQ *_req = nullptr;
    _req = X509_REQ_new();
    if (_req == nullptr)
    {
        std::cerr << "Не могу создать структуру X509_REQ: "
                  << ERR_reason_error_string(ERR_get_error()) << std::endl;
        return nullptr;
    }

    X509_REQ_PTR req(_req,
                     [](X509_REQ *cur) {
                         if (cur != nullptr)
                         {
                             X509_REQ_free(cur);
                             std::cout << "Структура CSR удалена\n";
                         }
                     });

    // Задаем версию
    if (X509_REQ_set_version(req.get(), 2) != 1)
    {
        std::cerr << "Не могу установить версию запроса: "
                  << ERR_reason_error_string(ERR_get_error()) << std::endl;
        return nullptr;
    }

    // Задаем поля
    X509_NAME *name = X509_REQ_get_subject_name(req.get());

    auto it = subj.begin();
    while (it != subj.end())
    {
        auto cur = *it;
        if (X509_NAME_add_entry_by_txt(name,
                                       cur.first.c_str(),
                                       MBSTRING_ASC,
                                       (const unsigned char *)cur.second.c_str(),
                                       -1, -1, 0) != 1)
        {
            std::cerr << "Не мозу задать поле "
                      << cur.first
                      << " со значением "
                      << cur.second << ": "
                      << ERR_reason_error_string(ERR_get_error());
            return nullptr;
        }
        it++;
    }

    //Устанавливаем открытый ключ
    if (X509_REQ_set_pubkey(req.get(), key.get()) != 1)
    {
        std::cerr << "Не могу установить открытый ключ для CSR:"
                  << ERR_reason_error_string(ERR_get_error()) << std::endl;
        return nullptr;
    }

    //Подпись сзапроса
    int size = X509_REQ_sign(req.get(), key.get(), EVP_sha256());
    if (size <= 0)
    {
        std::cerr << "Не могу подписать CSR:"
                  << ERR_reason_error_string(ERR_get_error()) << std::endl;
        return nullptr;
    }

    std::cout << "CSR создан. Длинна подписи: " << size << std::endl;

    return req;
}

bool SaveX509Req(X509_REQ_PTR csr, const std::string &fileName)
{
    if (csr == nullptr)
    {
        std::cerr << "Не задан CSR запрос для сохранения\n";
        return false;
    }

    if (fileName.empty())
    {
        std::cerr << "Не задано имя файла для записи\n";
        return false;
    }

    FILE *f = nullptr;
    f = fopen(fileName.c_str(), "wb");
    if (f == nullptr)
    {
        std::cerr << "Не могу открыть файл " << fileName << ": "
                  << strerror(errno) << std::endl;
        return false;
    }

    if (PEM_write_X509_REQ(f, csr.get()) != 1)
    {
        std::cerr << "Не могу записать CSR:"
                  << ERR_reason_error_string(ERR_get_error()) << std::endl;
        fclose(f);
        return false;
    }

    fclose(f);

    std::cout << "CSR успешно записан в " << fileName << std::endl;
    return true;
}

X509_REQ_PTR LoadCSR(const std::string &fileName)
{
    if (fileName.empty())
    {
        std::cerr << "Не задано имя файла для чтения\n";
        return nullptr;
    }

    FILE *f = nullptr;
    f = fopen(fileName.c_str(), "rb");
    if (f == nullptr)
    {
        std::cerr << "Не могу открыть файл " << fileName << ": "
                  << strerror(errno) << std::endl;
        return false;
    }

    X509_REQ *req = nullptr;
    req = PEM_read_X509_REQ(f, nullptr, nullptr, nullptr);

    if (req == nullptr)
    {
        std::cerr << "Не могу прочитать CSR:"
                  << ERR_reason_error_string(ERR_get_error()) << std::endl;
        fclose(f);
        return nullptr;
    }

    fclose(f);

    X509_REQ_PTR res(req,
                     [](X509_REQ *cur) {
                         if (cur != nullptr)
                         {
                             X509_REQ_free(cur);
                             std::cout << "Структура CSR удалена\n";
                         }
                     });

    std::cout << "CSR из файла " << fileName << " успешно загружен\n";

    return res;
}

X509_PTR GenerateCert(SSL_PKEY_PTR key, const SubjectMap &subj)
{
    if (key == nullptr)
    {
        std::cerr << "Не задан ключ для подписи сертификата\n";
        return nullptr;
    }

    if (subj.empty())
    {
        std::cerr << "Не заданы данные сертификата\n";
        return nullptr;
    }

    // Создаем структуру сертификата
    X509 *_cert = nullptr;
    _cert = X509_new();
    if (_cert == nullptr)
    {
        std::cerr << "Не могу создать структуру X509: "
                  << ERR_reason_error_string(ERR_get_error()) << std::endl;
        return nullptr;
    }

    X509_PTR cert(_cert,
                  [](X509 *cur) {
                      if (cur != nullptr)
                      {
                          X509_free(cur);
                          std::cout << "Структура сертификата удалена\n";
                      }
                  });

    // Задаем версию
    if (X509_set_version(cert.get(), 2) != 1)
    {
        std::cerr << "Не могу установить версию сертификата: "
                  << ERR_reason_error_string(ERR_get_error()) << std::endl;
        return nullptr;
    }
    //Задаем серийный номер
    ASN1_INTEGER *aserial = NULL;
    aserial = M_ASN1_INTEGER_new();
    ASN1_INTEGER_set(aserial, 1);
    if (X509_set_serialNumber(cert.get(), aserial) != 1)
    {
        std::cerr << "Не могу установить серийный номер сертификата: "
                  << ERR_reason_error_string(ERR_get_error()) << std::endl;
        return nullptr;
    }

    // Задаем поля
    X509_NAME *name = X509_get_subject_name(cert.get());

    if (name == NULL)
    {
        std::cerr << "Не могу получить структуру для записи данных издателя: "
                  << ERR_reason_error_string(ERR_get_error()) << std::endl;
        return nullptr;
    }

    auto it = subj.begin();
    while (it != subj.end())
    {
        auto cur = *it;
        if (X509_NAME_add_entry_by_txt(name,
                                       cur.first.c_str(),
                                       MBSTRING_ASC,
                                       (const unsigned char *)cur.second.c_str(),
                                       -1, -1, 0) != 1)
        {
            std::cerr << "Не мозу задать поле "
                      << cur.first
                      << " со значением "
                      << cur.second << ": "
                      << ERR_reason_error_string(ERR_get_error());
            return nullptr;
        }
        it++;
    }

    //Устанавливаем открытый ключ
    if (X509_set_pubkey(cert.get(), key.get()) != 1)
    {
        std::cerr << "Не могу установить открытый ключ для CSR:"
                  << ERR_reason_error_string(ERR_get_error()) << std::endl;
        return nullptr;
    }

    //Установка времени жизни сертификата
    if ((X509_gmtime_adj(X509_get_notBefore(cert.get()), 0)) == nullptr)
    {
        std::cerr << "Не могу установить дату начала действия сертификата: "
                  << ERR_reason_error_string(ERR_get_error()) << std::endl;
        return nullptr;
    }

    // 31536000 * 3 -> сертификат будет считаться действующим в течении 3х лет
    if ((X509_gmtime_adj(X509_get_notAfter(cert.get()), 31536000 * 3)) == nullptr)
    {
        std::cerr << "Не могу установить дату окончания действия сертификата: "
                  << ERR_reason_error_string(ERR_get_error()) << std::endl;
        return nullptr;
    }

    //Подпись сертификата
    int size = X509_sign(cert.get(), key.get(), EVP_sha256());
    if (size <= 0)
    {
        long e = ERR_get_error();
        if (e != 0)
        {
            std::cerr << "Не могу подписать CSR:"
                      << ERR_reason_error_string(ERR_get_error()) << std::endl;
            return nullptr;
        }
    }

    std::cout << "Сертификат создан. Длинна подписи: " << size << std::endl;

    return cert;
}

X509_PTR GenerateCert(SSL_PKEY_PTR ca_key, X509_PTR ca_cert, X509_REQ_PTR csr)
{
    if (ca_key == nullptr)
    {
        std::cerr << "Не задан закрытый ключ CA\n";
        return nullptr;
    }

    if (ca_cert == nullptr)
    {
        std::cerr << "Не задан сертификат CA\n";
        return nullptr;
    }

    if (csr == nullptr)
    {
        std::cerr << "Не задан CSR запрос\n";
        return nullptr;
    }

    // Создаем структуру сертификата
    X509 *_cert = nullptr;
    _cert = X509_new();
    if (_cert == nullptr)
    {
        std::cerr << "Не могу создать структуру X509: "
                  << ERR_reason_error_string(ERR_get_error()) << std::endl;
        return nullptr;
    }

    X509_PTR cert(_cert,
                  [](X509 *cur) {
                      if (cur != nullptr)
                      {
                          X509_free(cur);
                          std::cout << "Структура сертификата удалена\n";
                      }
                  });

    // Задаем версию
    if (X509_set_version(cert.get(), 2) != 1)
    {
        std::cerr << "Не могу установить версию сертификата: "
                  << ERR_reason_error_string(ERR_get_error()) << std::endl;
        return nullptr;
    }

    //Задаем серийный номер
    ASN1_INTEGER *aserial = NULL;
    aserial = M_ASN1_INTEGER_new();
    ASN1_INTEGER_set(aserial, 1);
    if (X509_set_serialNumber(cert.get(), aserial) != 1)
    {
        std::cerr << "Не могу установить серийный номер сертификата: "
                  << ERR_reason_error_string(ERR_get_error()) << std::endl;
        return nullptr;
    }

    //Задаем данные из CSR в новый сертификат
    X509_NAME *name = nullptr;
    name = X509_REQ_get_subject_name(csr.get());
    if (name == nullptr)
    {
        std::cerr << "Не могу прочитать данные из CSR: "
                  << ERR_reason_error_string(ERR_get_error()) << std::endl;
        return nullptr;
    }

    if (X509_set_subject_name(cert.get(), name) != 1)
    {
        std::cerr << "Не могу установить данные из CSR в сертификат: "
                  << ERR_reason_error_string(ERR_get_error()) << std::endl;
        return nullptr;
    }

    //Задаем данные издателя
    name = X509_get_subject_name(ca_cert.get());
    if (name == nullptr)
    {
        std::cerr << "Не могу прочитать данные издателя: "
                  << ERR_reason_error_string(ERR_get_error()) << std::endl;
        return nullptr;
    }

    if (X509_set_issuer_name(cert.get(), name) != 1)
    {
        std::cerr << "Не могу установить данные издателя в сертификат: "
                  << ERR_reason_error_string(ERR_get_error()) << std::endl;
        return nullptr;
    }

    //Получение открытого ключа из CSR запроса
    EVP_PKEY *_csr_key = nullptr;
    _csr_key = X509_REQ_get_pubkey(csr.get());
    if (_csr_key == nullptr)
    {
        std::cerr << "Не могу получить открытый ключ из CSR запроса: "
                  << ERR_reason_error_string(ERR_get_error()) << std::endl;
        return nullptr;
    }

    std::shared_ptr<EVP_PKEY> csr_key(_csr_key,
                                      [](EVP_PKEY *cur) {
                                          if (cur != nullptr)
                                          {
                                              EVP_PKEY_free(cur);
                                              std::cout << "Структура для хранения ключей удалена\n";
                                          }
                                      });

    //Проверяем, что CSR был подписан этим ключом
    if (X509_REQ_verify(csr.get(), csr_key.get()) != 1)
    {
        std::cerr << "Неверная подпись CSR запроса: "
                  << ERR_reason_error_string(ERR_get_error()) << std::endl;
        return nullptr;
    }


    //Устанавливаем открытый ключ в новый сертификат
    if (X509_set_pubkey(cert.get(), csr_key.get()) != 1)
    {
        std::cerr << "Не могу установить открытый ключ в сертификат: "
                  << ERR_reason_error_string(ERR_get_error()) << std::endl;
        return nullptr;
    }

    //Установка времени жизни сертификата
    if ((X509_gmtime_adj(X509_get_notBefore(cert.get()), 0)) == nullptr)
    {
        std::cerr << "Не могу установить дату начала действия сертификата: "
                  << ERR_reason_error_string(ERR_get_error()) << std::endl;
        return nullptr;
    }

    // 31536000 * 3 -> сертификат будет считаться действующим в течении 3х лет
    if ((X509_gmtime_adj(X509_get_notAfter(cert.get()), 31536000 * 3)) == nullptr)
    {
        std::cerr << "Не могу установить дату окончания действия сертификата: "
                  << ERR_reason_error_string(ERR_get_error()) << std::endl;
        return nullptr;
    }

    //Подпись сертификата
    int size = X509_sign(cert.get(), ca_key.get(), EVP_sha256());
    if (size <= 0)
    {
        long e = ERR_get_error();
        if (e != 0)
        {
            std::cerr << "Не могу подписать CSR:"
                      << ERR_reason_error_string(ERR_get_error()) << std::endl;
            return nullptr;
        }
    }

    std::cout << "Сертификат создан. Длинна подписи: " << size << std::endl;

    return cert;
}

bool SaveCert(X509_PTR cert, const std::string &fileName)
{
    if (cert == nullptr)
    {
        std::cerr << "Не задан сертификат для сохранения\n";
        return false;
    }

    if (fileName.empty())
    {
        std::cerr << "Не задано имя файла для записи\n";
        return false;
    }

    FILE *f = nullptr;
    f = fopen(fileName.c_str(), "wb");
    if (f == nullptr)
    {
        std::cerr << "Не могу открыть файл " << fileName << ": "
                  << strerror(errno) << std::endl;
        return false;
    }

    if (PEM_write_X509(f, cert.get()) != 1)
    {
        std::cerr << "Не могу записать сертификат:"
                  << ERR_reason_error_string(ERR_get_error()) << std::endl;
        fclose(f);
        return false;
    }

    fclose(f);

    std::cout << "Сертификат успешно записан в " << fileName << std::endl;
    return true;
}

X509_PTR LoadCert(const std::string &fileName)
{
    if (fileName.empty())
    {
        std::cerr << "Не задано имя файла для чтения\n";
        return nullptr;
    }

    FILE *f = nullptr;
    f = fopen(fileName.c_str(), "rb");
    if (f == nullptr)
    {
        std::cerr << "Не могу открыть файл " << fileName << ": "
                  << strerror(errno) << std::endl;
        return false;
    }

    X509 *_cert = nullptr;
    _cert = PEM_read_X509(f, nullptr, nullptr, nullptr);

    if (_cert == nullptr)
    {
        std::cerr << "Не могу прочитать сертификат:"
                  << ERR_reason_error_string(ERR_get_error()) << std::endl;
        fclose(f);
        return nullptr;
    }

    fclose(f);

    X509_PTR res(_cert,
                 [](X509 *cur) {
                     if (cur != nullptr)
                     {
                         X509_free(cur);
                         std::cout << "Структура сертификата удалена\n";
                     }
                 });

    std::cout << "Сертификат из файла " << fileName << " успешно загружен\n";

    return res;
}

int GetListenSocket(uint16_t port)
{
    int inSock = 0;
    inSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (inSock <= 0)
    {
        std::cerr << "Не могу создать сокет: " << strerror(errno) << std::endl;
        return -1;
    }

    struct sockaddr_in local;
    memset(&local, 0, sizeof(struct sockaddr_in));
    local.sin_family      = AF_INET;
    local.sin_port        = htons(port);
    local.sin_addr.s_addr = INADDR_ANY;

    if (bind(inSock, (const sockaddr *)&local, sizeof(struct sockaddr_in)) != 0)
    {
        std::cerr << "Не могу привязат сокет к порту: " << strerror(errno) << std::endl;
        return -1;
    }

    if (listen(inSock, 5) != 0)
    {
        std::cerr << "Не могу начать слушать на сокете: " << strerror(errno) << std::endl;
        return -1;
    }

    std::cout << "Сокет ожидает подключения на " << port << " порту\n";

    return inSock;
}

int ConnectToHost(const std::string &host, uint16_t port)
{
    if (host.empty())
    {
        std::cerr << "Не задан хост\n";
        return -1;
    }

    std::cout << "Подключаемся к " << host << " на порту " << port << std::endl;

    //Попробуем получить IP для хоста
    struct hostent *ip = nullptr;
    ip = gethostbyname(host.c_str());
    if (ip == nullptr)
    {
        std::cerr << "Не могу получить IP Для хоста " << host << std::endl;
        return -1;
    }

    //Создаем сокет
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == -1)
    {
        std::cerr << "Не могу получить сокет: " << strerror(errno) << std::endl;
        return -1;
    }

    struct sockaddr_in dest_addr;
    memset(&dest_addr, 0, sizeof(struct sockaddr_in));
    dest_addr.sin_family      = AF_INET;
    dest_addr.sin_port        = htons(port);
    dest_addr.sin_addr.s_addr = *(long *)(ip->h_addr);

    //Подключаемся
    if (connect(sock, (struct sockaddr *)&dest_addr, sizeof(struct sockaddr)) == -1)
    {
        std::cerr << "Не могу подключиться к хосту "
                  << host << "[" << inet_ntoa(dest_addr.sin_addr) << "]:" << port
                  << " Ошибка: " << strerror(errno) << std::endl;
        return -1;
    }

    std::cout << "Подключен к хосту " << host << "[" << inet_ntoa(dest_addr.sin_addr) << "]:" << port << std::endl;

    return sock;
}

bool MakeSocketNonblocking(int sock)
{
    int flags = 0;
    if ((flags = fcntl(sock, F_GETFL, 0)) < 0)
    {
        std::cerr << "Не могу получить флаги сокета: " << strerror(errno) << std::endl;
        return false;
    }
    if (fcntl(sock, F_SETFL, flags | O_NONBLOCK) < 0)
    {
        std::cerr << "Не могу установить флаги сокета: " << strerror(errno) << std::endl;
        return false;
    }

    return true;
}

} // namespace Taigasystem