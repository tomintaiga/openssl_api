#include <iostream>
#include <sys/socket.h>
#include "tools.h"
#include <openssl/err.h>

void ProxyData(const char *data, ssize_t size)
{
    if (!data)
    {
        std::cerr << "Нет данных для обработки" << std::endl;
        return;
    }

    if (!size)
    {
        std::cerr << "Данные для обработки имеют длинну: " << size << std::endl;
        return;
    }

    //Просто выведем на экран данные
    std::string str(data, (unsigned long)size);
    std::cout << str << std::endl;
}

int main()
{
    //Следит за инициализацией и очисткой библиотеки OpenSSL
    Taigasystem::OpenSSLInitializer initializer;

    const int         server_port  = 5566;
    const std::string ca_cert_file = "ca.crt";

    //Будем подключаться по этому адресу
    const std::string dest_host   = "taigasystem.com";
    const int         dest_port   = 443;
    const int         buffer_size = 4096;

    std::cout << "Для проверки воспользуйтесь командой 'curl --cacert "
              << ca_cert_file << " -v https://127.0.0.1:" << server_port
              << " -H \"Host: " << dest_host << "\" '" << std::endl;

    //Ключ для CA
    Taigasystem::SSL_PKEY_PTR ca_key = Taigasystem::GenerateKey();

    Taigasystem::SubjectMap ca_subj {
        {"C", "RU"},
        {"ST", "Moscow"},
        {"L", "Moscow"},
        {"O", "Taigasystem"},
        {"CN", "127.0.0.1"}};

    Taigasystem::X509_PTR ca_cert = Taigasystem::GenerateCert(ca_key, ca_subj);

    //Ключ для хоста
    Taigasystem::SSL_PKEY_PTR server_key = Taigasystem::GenerateKey();
    Taigasystem::SubjectMap server_subj {
        {"C", "RU"},
        {"ST", "Moscow"},
        {"L", "Moscow"},
        {"O", "Taigasystem"},
        {"CN", "127.0.0.1"}};

    //Запрос сертификата
    Taigasystem::X509_REQ_PTR server_req  = Taigasystem::GenerateCSR(server_key, server_subj);

    //Сертификат
    Taigasystem::X509_PTR server_cert = Taigasystem::GenerateCert(ca_key, ca_cert, server_req);
    //Сохраняем
    if (!Taigasystem::SaveCert(server_cert, ca_cert_file))
        return 1;

    //Запускаем сервер
    Taigasystem::SSL_CTX_PTR serverContext = Taigasystem::CreateServerContext();

    //Задаем сертификат и ключь для контекста
    if (SSL_CTX_use_certificate(serverContext.get(), server_cert.get()) != 1)
    {
        std::cerr << "Не могу установить сертификат сервера в контекст:"
                  << ERR_reason_error_string(ERR_get_error()) << std::endl;
        return 1;
    }

    if (SSL_CTX_use_PrivateKey(serverContext.get(), server_key.get()) != 1)
    {
        std::cerr << "Не могу установить закрытый ключ сервера в контекст:"
                  << ERR_reason_error_string(ERR_get_error()) << std::endl;
        return 1;
    }

    int inSocket = Taigasystem::GetListenSocket(server_port);
    if (inSocket == -1)
        return 1;

    int serverSocket = -1;
    int clientSocket = -1;

    while (true)
    {
        auto      *addr = new sockaddr;
        socklen_t len   = sizeof(struct sockaddr);

        serverSocket = accept(inSocket, addr, &len);
        if (serverSocket == -1)
        {
            std::cerr << "Не могу получить входящий сокет: " << strerror(errno) << std::endl;
            break;
        }

        std::cout << "Новое входящие соединение\n";

        Taigasystem::SSL_PTR serverSSL = Taigasystem::GetSSL(serverContext);
        if (serverSSL == nullptr)
        {
            std::cerr << "Нет SSL структуры\n";
            break;
        }

        //Устанавливаем сокет в SSL
        if (SSL_set_fd(serverSSL.get(), serverSocket) != 1)
        {
            std::cerr << "Не могу установить сокет в SSL:"
                      << ERR_reason_error_string(ERR_get_error()) << std::endl;
            break;
        }

        /**
         * Осуществляем SSL handshake
         *
         * Если бы сокет был не блокирующим, то пришлось бы вызывать эту функцию несколько раз
         * и смотреть что она возвращает. Это происходит изза того, что Handshake включает в
         * себя несколько сообщений которыми должны обменяться клиент и сервер
         */
        if (SSL_accept(serverSSL.get()) != 1)
        {
            std::cerr << "Не могу осуществить handshake:"
                      << ERR_reason_error_string(ERR_get_error()) << std::endl;
            break;
        }

        //Переводим серверный сокет в неблокирующее состояние
        if (!Taigasystem::MakeSocketNonblocking(serverSocket))
        {
            std::cerr << "Не могу перевести серверный сокет в блокирующее состояние\n";
            break;
        }

        //Теперь нужно подключиться к удаленному SSL серверу.
        clientSocket = Taigasystem::ConnectToHost(dest_host, dest_port);
        if (clientSocket == -1)
            break;

        //Создаем контекст для клиента
        Taigasystem::SSL_CTX_PTR clientContext = Taigasystem::CreateClientContext();
        if (clientContext == nullptr)
            break;

        Taigasystem::SSL_PTR clientSSL = Taigasystem::GetSSL(clientContext);
        if (clientSSL == nullptr)
            break;

        //Устанавливаем сокет для клиентского SSL
        if (SSL_set_fd(clientSSL.get(), clientSocket) != 1)
        {
            std::cerr << "Не могу установить сокет в SSL:"
                      << ERR_reason_error_string(ERR_get_error()) << std::endl;
            break;
        }

        //Клиентский handshake
        if (SSL_connect(clientSSL.get()) != 1)
        {
            std::cerr << "Не могу установить соединение с "
                      << dest_host << ":" << dest_port << " :"
                      << ERR_reason_error_string(ERR_get_error()) << std::endl;
            break;
        }

        std::cout << "SSL соединение с " << dest_host << ":" << dest_port << " установлено\n";

        if (!Taigasystem::MakeSocketNonblocking(clientSocket))
        {
            std::cerr << "Не могу перевести клиентский сокет в блокирующее состояние\n";
            break;
        }

        std::cout << "Цикл чтения/записи данных\n";

        //Обмен данными
        time_t last_act = time(NULL);
        char   buf[buffer_size];
        while (true)
        {
            memset(buf, 0, buffer_size);

            //Читаем с подключенного клиента
            int len = SSL_read(serverSSL.get(), buf, buffer_size);
            if (len > 0)
            {
                last_act = time(NULL);
                std::cout << "#######################################\n";
                std::cout << "Прочитано " << len << " байт от клиента\n";
                std::cout << "#######################################\n";
                ProxyData(buf, len);
                if (SSL_write(clientSSL.get(), buf, buffer_size) < 0) //Отправляем данные серверу
                {
                    std::cerr << "Не могу отправить данные серверу: "
                              << ERR_reason_error_string(ERR_get_error()) << std::endl;
                    break;
                }
            }

            //Читаем ответ сервера
            memset(buf, 0, buffer_size);
            len = SSL_read(clientSSL.get(), buf, buffer_size);
            if (len > 0)
            {
                last_act = time(NULL);
                std::cout << "#######################################\n";
                std::cout << "Прочитано " << len << " байт от сервера\n";
                std::cout << "#######################################\n";
                ProxyData(buf, len);
                if (SSL_write(serverSSL.get(), buf, buffer_size) < 0) //Отправляем данные клиенту
                {
                    std::cerr << "Не могу отправить данные клиенту: "
                              << ERR_reason_error_string(ERR_get_error()) << std::endl;
                    break;
                }
            }

            //Проверка тайм-аута
            if (time(NULL) - last_act > 3)
            {
                std::cout << "Тайм-аут соединения\n";
                break;
            }

        }

        std::cout << "Конец цикла чтения/записи данных\n";
        break;
    }

    close(inSocket);
    close(serverSocket);
    close(clientSocket);

    return 0;
}
