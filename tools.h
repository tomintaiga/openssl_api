#ifndef OPENSSL_API_TOOLS_H
#define OPENSSL_API_TOOLS_H

#include <openssl/ssl.h>
#include <openssl/x509.h>
#include <memory>
#include <string>
#include <map>
#include <unistd.h>
#include <iostream>

/**
 * @namespace Taigasystem
 * @brief Пространство имен программы
 */
namespace Taigasystem {

//################################################################
//                 Общие для всей библиотеки функции
//################################################################

/**
 * @brief Выполняет инициализацию юилиотеки OpenSSL
 *
 * После выполнения функции, можно использовать все функции библиотеки OpenSSL
 */
void InitOpenSSL();

/**
 * @brief Выполняет очистку используемых библиотекой даных
 */
void ClearOpenSSL();

/**
 * @brief Класс помошник, на подобии std::lock_guard
 *
 * При создании объекта, библиотека OpenSSL будет инициализирована,
 * а при удалении очищена
 */
class OpenSSLInitializer
{
  public:
    OpenSSLInitializer() { InitOpenSSL(); }

    ~OpenSSLInitializer() { ClearOpenSSL(); }
};

//################################################################
//                 SSL контекст
//################################################################

typedef std::shared_ptr<SSL>     SSL_PTR;
typedef std::shared_ptr<SSL_CTX> SSL_CTX_PTR;
/**
 * @brief Создает серверный контекст
 * @return Созданный контекст или nullptr в случае ошибки
 */
SSL_CTX_PTR CreateServerContext();

/**
 * @brief Создает клиентский контекст
 * @return Созданный контекст или nullptr в случае ошибки
 */
SSL_CTX_PTR CreateClientContext();

/**
 * @brief Создает контекст с использованием указанного SSL метода
 * @param method Метод для контекста
 * @return Созданный контекст или nullptr в случае ошибки
 */
SSL_CTX_PTR CreateContext(const SSL_METHOD *method);

/**
 * @brief Получает из контекста SSL структуру
 * @param ctx SSL контекст
 * @return Полученная структура или nullptr в случае ошибки
 */
SSL_PTR GetSSL(SSL_CTX_PTR ctx);

//################################################################
//                 Работа с ключами
//################################################################

typedef std::shared_ptr<EVP_PKEY> SSL_PKEY_PTR;

/**
 * @brief Создает закрытый и открытый ключи и упаковывает их в структуру EVP_PKEY
 * @param size Размер ключа
 * @return Созданные ключи или nullptr в случае ошибки
 */
SSL_PKEY_PTR GenerateKey(int size = 4096);

/**
 * @brief Сохраняет закрытый ключ в файл в формате PEM
 * @param key Ключ для сохранения
 * @param fileName Имя файла для записи
 * @param passwd Пароль для шифрования ключа. Если оставить пустым - ключ
 * будет записан без шифрования
 * @return true если запись прошла успешно и false если нет
 */
bool SaveKey(SSL_PKEY_PTR key, const std::string &fileName, const std::string &passwd = "");

/**
 * @brief Загружает ключ из PEM файла
 * @param fileName Имя файла
 * @param passwd Пароль от зашифрованного ключа. Если оставить пустым - считаем
 * что ключ не зашифрован
 * @return Загруженный ключ или nullptr при невозможности загрузки
 */
SSL_PKEY_PTR LoadKey(const std::string &fileName, const std::string &passwd = "");

//################################################################
//                 Работа с CSR
//################################################################

typedef std::shared_ptr<X509_REQ>          X509_REQ_PTR;
typedef std::map<std::string, std::string> SubjectMap;

/**
 * @brief Создает запрос сертификата при помощь ключа и набора данных о сертификате
 * @param key Ключ для подписи запроса
 * @param subj Данные сертификата
 * @return Созданный запрос сертификата или nullptr в случае ошибки
 *
 * В качестве ключей в данных используются значения С, ST, L, O, CN
 */
X509_REQ_PTR GenerateCSR(SSL_PKEY_PTR key, const SubjectMap &subj);

/**
 * @brief Сохраняет CSR в PEM файл
 * @param csr CSR Для сохранения
 * @param fileName Имя файла для записи
 * @return true если запись прошла успешно и false если нет
 */
bool SaveX509Req(X509_REQ_PTR csr, const std::string &fileName);

/**
 * @brief Загружает CSR из файла
 * @param fileName Имя файла
 * @return Загруженный CSR или nullptr в случе ошибки
 */
X509_REQ_PTR LoadCSR(const std::string &fileName);

//################################################################
//                 Работа с сертификатами
//################################################################

typedef std::shared_ptr<X509> X509_PTR;

/**
 * @brief Генерирует сертификат без CSR (самоподписанный)
 * @param key Ключ для подписи сертификата
 * @param subj Данные сертификата
 * @return Созданный сертификат или nullptr в случае ошибки
 */
X509_PTR GenerateCert(SSL_PKEY_PTR key, const SubjectMap &subj);

/**
 * @brief Генерирует сертификат по CSR
 * @param ca_key Закрытый ключ удостоверяющего центра
 * @param ca_cert Сертификат удостоверяющего центра
 * @param csr CSR запрос сертификата
 * @return Созданный сертификат или nullptr в случае ошибки
 */
X509_PTR GenerateCert(SSL_PKEY_PTR ca_key, X509_PTR ca_cert, X509_REQ_PTR csr);

/**
 * @brief Сохраняет сертификат в файл в PEM формате
 * @param cert Сертификат для сохранения
 * @param fileName Имя файла для записи данных
 * @return true если сохранение прошло успешно и false если нет
 */
bool SaveCert(X509_PTR cert, const std::string &fileName);

/**
 * @brief Загрузка сертификата из файла
 * @param fileName Имя файла
 * @return Загруженный сертификат или nullptr в случае ошибки
 */
X509_PTR LoadCert(const std::string &fileName);

//################################################################
//                 Сетевые компоненты
//################################################################

/**
 * @brief Открывает TCP сокет на указанном порту
 * @param port Порт, который нужно слушать
 * @return Открытый сокет или -1 в случае ошибки
 */
int GetListenSocket(uint16_t port);

/**
 * @brief Пдключиться по указанным хосту и порту по протоколу TCP
 * @param host Хост для подключения
 * @param port Порт для подключения
 * @return Подключенный сокет или -1 если подключение не удалось
 */
int ConnectToHost(const std::string &host, uint16_t port);

/**
 * @brief Переводим сокет в неблокирующий режим
 * @param sock Сокет
 * @return true если перевод удался и false если нет
 */
bool MakeSocketNonblocking(int sock);

} // namespace Taigasystem

#endif //OPENSSL_API_TOOLS_H
